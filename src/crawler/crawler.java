package crawler;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sound.midi.Synthesizer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


/**
 * Servlet implementation class crawler
 */
@WebServlet("/crawler")
public class crawler extends HttpServlet {
	ArrayList<String> synonym = new ArrayList();
	ArrayList<String> synonymForest = new ArrayList();
	ArrayList<String> kwuntung = new ArrayList();
       


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");//設定網頁丟出需求時，用UTF-8編碼
		response.setCharacterEncoding("UTF-8");//列出訊息時，用UTF-8編碼
		if(request.getParameter("text") != null) {
			kwuntung(request.getParameter("text"));
			sinica(request.getParameter("text"));
			response.getWriter().append("東東同義詞:<br>");
			for(String text : kwuntung)
				response.getWriter().append(text+"<br>");
			response.getWriter().append("<br>中研院同義詞:<br>");
			for(String text : synonym)
				response.getWriter().append(text+"<br>");
			response.getWriter().append("<br>中研院同義詞林:<br>");
			for(String text : synonymForest)
				response.getWriter().append(text+"<br>");
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}
	private void revised(String searchName) {
		try {
			Document  body = Jsoup.connect("http://dict.revised.moe.edu.tw/cgi-bin/cbdic/gsweb.cgi")
					.data("qs0",URLEncoder.encode(searchName,"UTF-8"))
					.data("o","e0")
					.data("ccd","Iu1PuY")
					.data("sec","sec1")
					.data("selectmode","mode1")
					.data("button.y","15")
					.data("&button.x","28")
					.post();
			System.out.print(body);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private synchronized void kwuntung(String searchName){
		kwuntung.clear();
		try {
			Document  body = Jsoup.connect("http://www.kwuntung.net/synonym/synonym_result2.php?Sense="+URLEncoder.encode(searchName,"big5")).get();
			Elements text = body.select(".text");
			System.out.println(text.size());
			for(int index = 0 ; index < text.size();index++) {
				String[] array = text.get(index).text().split("\\u00a0\\u00a0");
				for(String textarray : array) {
					if(textarray.equalsIgnoreCase("\\")) {
						
					}else {
						kwuntung.add(textarray);
					}
				}
			}
//			System.out.print("http://www.kwuntung.net/synonym/synonym_result.php?Sense="+URLEncoder.encode(searchName,"big5"));
//			System.out.print(text.text());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private synchronized void sinica(String searchName) {
		synonym.clear();
		synonymForest.clear();
		try {
			Document  body = Jsoup.connect("http://proj1.sinica.edu.tw/~swjz/ftms-bin/scripts/look_for_sym.pl")
					.data("kw0",URLEncoder.encode(searchName,"big5"))
					.post();

			Elements aString =body.select("tr");
			int trIndex = 0;//1為﹝國語彙詞典〕、2為﹝同義詞林〕
			for(int index = 0 ; index < aString.size();index++) {
				if(aString.get(index).text().equalsIgnoreCase("﹝國語彙詞典〕"))
					trIndex = 1;
				else if(aString.get(index).text().equalsIgnoreCase("﹝同義詞林〕")) 
					trIndex = 2;
				else if(aString.get(index).text().contains("(")&&aString.get(index).text().contains(") ")) {
					String text = aString.get(index).text();
					String newText = text.substring(text.indexOf(") ")+2,text.length());
					if(trIndex ==1) {
						synonym.add(newText);
					}else if(trIndex==2) {
						synonymForest.add(newText);
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
