package frontier;

import java.awt.event.ItemEvent;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

/**
 * Servlet implementation class test
 */
@WebServlet("/test")
public class test extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(!isrun) {
			final readDB db = new readDB();
			final String[] expool=getQuestion(db);
			arraySize = expool.length;
			for(int index = 0 ; index < 1 ; index++) {
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						progress(db,expool,threadIndex++);
					}
				}).start();
			}
		}
//		runThread.start();
		response.getWriter().append("Served at: "+isrun +":"+ threadIndex +"/"+ arraySize);
	}

	/** 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
//	private Runnable runnable = new Runnable() {
//		
//		@Override
//		public void run() {
//			String[] expool=getQuestion();
//			arraySize = expool.length;
//			for(String item : expool) {
//				String[][] jiebaStr = jieba(item);
//				String con ="";
//				String pos ="";
//				for(int index = 0 ; jiebaStr != null && jiebaStr.length>0 &&index < jiebaStr[0].length ; index++) {
//					if(con.length() > 0) {
//						con += "、";
//						pos += "、";
//					}
//					con += jiebaStr[0][index];
//					pos += jiebaStr[1][index]; 
//				}
//			writeSQL(item,con,pos);
//			}	
//		}
//	};
	private static int threadIndex = 0 ;
	private static int arraySize = 0;
	private static boolean isrun = false;
	private void progress(readDB db,String[] expool ,int arrayIndex) {
		System.out.print(threadIndex +"/"+ arraySize);
		String[][] jiebaStr = jieba(expool[arrayIndex]);
		String con ="";
		String pos ="";
		for(int index = 0 ; jiebaStr != null && jiebaStr.length>0 &&index < jiebaStr[0].length ; index++) {
			if(con.length() > 0) {
				con += "、";
				pos += "、";
			}
			con += jiebaStr[0][index];
			pos += jiebaStr[1][index]; 
		}
		writeSQL(db,expool[arrayIndex],con,pos);
		if(threadIndex < arraySize) {//還沒跑完
			progress(db,expool,threadIndex++);
		}
		if(arrayIndex >= arraySize-1) {//已經跑完
		
			isrun = false;
			threadIndex = 0;
			db.closeDB();
			db = null;
		}
	}
//	private Thread runThread = new Thread(runnable);
	//取得jieba文字和詞性
	private String[][] jieba(String text) {
		try {
			String body = Jsoup.connect("http://192.168.10.216/jieba/cut/api/v1.0/"+URLEncoder.encode(text,"UTF-8")).execute().body();
			//http://192.168.10.216/pyltp/api/v1.0/
			System.out.println(body);
			try {
				JSONObject object = new JSONObject(body);
				JSONArray content = object.getJSONArray("cont");
				JSONArray pos = object.getJSONArray("pos");
				String[][] demolition = new String[2][content.length()];
				for(int index = 0 ; index < content.length() ; index++) {
					demolition[0][index] = content.get(index).toString();
					demolition[1][index] = pos.get(index).toString();
				}
				return demolition;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	//查詢題庫內所有的字
	private String[] getQuestion(readDB db) {
		ArrayList<HashMap<String, String>> expool = db.getDBresult("SELECT `word` FROM `as_expool`");
		String[] item = new String[expool.size()];
		for(int index = 0 ; index < expool.size() ; index++)  {
			item[index] = expool.get(index).get("word");
		}

		return item;
	}
	//寫入及修改資料庫
	private void writeSQL(readDB db,String word,String jiebacon , String jiebapos) {
		//沒有資料時
		ArrayList<HashMap<String, String>> expool = db.getDBresult("SELECT * FROM as_jieba where word = '"+word+"'");
		if(expool.size()>0) {
//			System.out.println("UPDATE");
//			System.out.println(expool.get(0));
//			System.out.println(expool.get(0).get("solution"));
			System.out.println("UPDATE `as_jieba` SET jiebacon='"+jiebacon+"',jiebapos='"+jiebapos+"',correct="+true+" where word = '"+word+"'");
			if(expool.get(0).get("solution").equalsIgnoreCase(jiebacon)) {//資料庫的解答和jieba一樣
				db.writeDB("UPDATE `as_jieba` SET jiebacon='"+jiebacon+"',jiebapos='"+jiebapos+"',correct="+true+" where word = '"+word+"'");			
			}else {
				db.writeDB("UPDATE `as_jieba` SET jiebacon='"+jiebacon+"',jiebapos='"+jiebapos+"',correct="+false+" where word = '"+word+"'");
			}
		}else {//沒有筆數要新增
			System.out.println("insert");
//			System.out.println("INSERT INTO as_jieba(word, solution, jiebacon, jiebapos, correct) VALUES ("+word+",'',"+jiebacon+","+jiebapos+","+false+")");
			db.writeDB("INSERT INTO as_jieba(word, solution,jiebacon, jiebapos, correct) VALUES ('"+word+"','','"+jiebacon+"','"+jiebapos+"',"+false+")");
		}
		
	}
}
