package frontier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class KnowledgeMap
 */
@WebServlet("/KnowledgeMap")
public class KnowledgeMap extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");// 設定網頁丟出需求時，用UTF-8編碼
		response.setCharacterEncoding("UTF-8");// 列出訊息時，用UTF-8編碼
		if (request.getParameter("test") != null) {
			response.getWriter().append("阿拉伯數字:"+isNumeric(request.getParameter("test")));
			response.getWriter().append("<br>國字數字:"+isChNumeric(request.getParameter("test")));
			response.getWriter().append("<br>有包含國字小數:"+isPointNumeric(request.getParameter("test")));
			response.getWriter().append("<br>小數轉數字:"+chPointToPoint(request.getParameter("test")));
			
		}
		if (request.getParameter("str") != null) {
			JSONObject print = new JSONObject();
			String[] strArray = request.getParameter("str").split(",");// 將文字拆成個別搜尋詞
			for (String str : strArray) {
				JSONArray array = new JSONArray();// 放搜尋詞全部結果
				if(isNumeric(str)) {
					
				}else if(isChNumeric(str)) {
					
				}
				// 原本的搜尋詞
				JSONObject[] strObject = getSqlSearch(str);
				if (strObject != null)
					for(JSONObject object : strObject)
						array.put(object);
				// 將搜尋詞 阿拉伯數字轉國字數字
				String ma = getMath(str);
				String ch = getChMath(str);
				if (ma.length() > 0) {
					String intStr = intToStr(Integer.parseInt(ma), 0);
					String DateintStr = DateintToStr(Integer.parseInt(ma));
					JSONObject[] intStrObject = getSqlSearch(str.replace(ma, intStr));
					JSONObject[] DateintStrObject = getSqlSearch(str.replace(ma, DateintStr));
					if (intStrObject != null)
						for(JSONObject object : intStrObject)
							array.put(object);
					if (DateintStrObject != null)
						for(JSONObject object : DateintStrObject)
							array.put(object);
				}
				// 將搜尋詞 國字數字轉阿拉伯數字
				if (ch.length() > 0) {
					String strInt = StrToInt(ch) + "";
					JSONObject[] strIntObject = getSqlSearch(str.replace(ch, strInt));
					if (strIntObject != null)
							for(JSONObject object : strIntObject)
						array.put(object);
				}
				try {
					print.put(str, array);
				} catch (JSONException e) {

				}
			}
			response.getWriter().append(print.toString());
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	private boolean isNumeric(String s) {  
        if (s != null && !"".equals(s.trim())) {
        	if(s.matches("^[0-9.]*$"))
        		if(s.substring(s.length()-1,s.length()).equalsIgnoreCase(".")||s.substring(0,1).equalsIgnoreCase("."))//頭尾不能是.
        			return false;
        		else
        			return true; 
        } 
            return false;  
    } 
	private boolean isChNumeric(String s) {  
        if (s != null && !"".equals(s.trim()))  
            return s.matches("^[零一二兩三四五六七八九十百千萬億兆]*$");  
        else  
            return false;  
    }  
	private boolean isPointNumeric(String s) {  
        if (s != null && !"".equals(s.trim()))  {
            return s.matches(".*[零一二兩三四五六七八九十百千萬億兆][點][零一二兩三四五六七八九]{1}.*");  
        }else  
            return false;  
    }
	//將字串所有的國字小數轉阿拉伯小數
	private String chPointToPoint(String pointString) {
		while (isPointNumeric(pointString)) {//有小數點
			String chpoint = getChPointMath(pointString);//找出第一組小數點
			String[] chpointArray = chpoint.split("點");//用點拆成兩個數字
			chpointArray[0] =  StrToInt(chpointArray[0])+"";//轉成數字
			chpointArray[1] =  StrToInt(chpointArray[1])+"";//轉成數字
			String newString = chpointArray[0]+"."+chpointArray[1];//組合成新的小數點數字
			pointString = pointString.replace(chpoint, newString);//取代掉原本的國字小數
		}
		return pointString;
	}
	private JSONObject[] getSqlSearch(String value) {
		final String databaseName = "learn";
		final String tableName = "triple";
		final String columnName = "vdkey";
		final String attributesName = "wordtag";
		readDB db = new readDB(databaseName);
		ArrayList<HashMap<String, String>> result = db
				.getDBresult("SELECT * FROM " + tableName + " where " + columnName + " = '" + value + "'");
		System.out.print("SELECT * FROM " + tableName + " where " + columnName + " = '" + value + "'");
		if (result.size() > 0) {
			JSONObject[] object = new JSONObject[result.size()];
			for(int index = 0 ; index <result.size();index++) {
				object[index] = new JSONObject();
			try {
				object[index].put("key", result.get(index).get(columnName));
				object[index].put("attributes", result.get(index).get(attributesName));
			} catch (JSONException e) {
			}
			}
			return object;
		}
		return null;
	}

	// 找出字串中第一組阿拉伯數字
	private String getMath(String value) {
		String math = "0123456789";
		String str = "";
		for (int index = 0; index < value.length(); index++) {
			if (math.contains(value.substring(index, index + 1))) {
				str += value.substring(index, index + 1);
			} else if (str.length() > 0) {
				return str;
			}
		}
		return str;
	}

	// 找出字串中第一組國字數字
	private String getChMath(String value) {
		String ch = "零一二兩三四五六七八九兩十百千萬億兆";
		String str = "";
		for (int index = 0; index < value.length(); index++) {
			if (ch.contains(value.substring(index, index + 1))) {
				str += value.substring(index, index + 1);
			} else if (str.length() > 0) {
				return str;
			}
		}
		// 單純只有單位的話 不算一組國字數字
		if (str.equalsIgnoreCase("十") || str.equalsIgnoreCase("百") || str.equalsIgnoreCase("千")
				|| str.equalsIgnoreCase("萬") || str.equalsIgnoreCase("億") || str.equalsIgnoreCase("兆"))
			return "";
		return str;
	}
	// 找出字串中第一組國字小數數字
	private String getChPointMath(String value) {
		String ch = "零一二兩三四五六七八九兩十百千萬億兆點";
		String str = "";
		for (int index = 0; index < value.length(); index++) {
			if (ch.contains(value.substring(index, index + 1))) {
				str += value.substring(index, index + 1);
			} else if (str.length() > 0) {
				return str;
			}
		}
		// 單純只有單位的話 不算一組國字數字
		if (str.equalsIgnoreCase("十") || str.equalsIgnoreCase("百") || str.equalsIgnoreCase("千")
				|| str.equalsIgnoreCase("萬") || str.equalsIgnoreCase("億") || str.equalsIgnoreCase("兆"))
			return "";
		if(!str.contains("點")||str.substring(str.length()-1,str.length()).equalsIgnoreCase("點")||str.substring(0,1).equalsIgnoreCase("點"))
			return "";
		return str;
	}

	// 國字轉數字
	private static long StrToInt(String value1) {
		int unitPosition1 = value1.indexOf("萬");
		int unitPosition2 = value1.indexOf("億");
		int unitPosition3 = value1.indexOf("兆");
		long mathValueTotal = 0;
		ArrayList<String> unitMath = new ArrayList();// X X萬X億X兆
		unitMath.add(value1);
		// 複製if else可擴充單位unitPosition....
		if (unitPosition3 > 0) {
			String[] unitStr = unitMath.get(0).split("兆");
			unitMath.set(0, unitStr[1]);// 將value刪除兆
			unitMath.add(1, unitStr[0]);// 兆前面的數字往後塞
		} else {
			unitMath.add(1, "零");// 沒有兆則塞零
		}
		if (unitPosition2 > 0) {
			String[] unitStr = unitMath.get(0).split("億");
			unitMath.set(0, unitStr[1]);// 將value刪除億
			unitMath.add(1, unitStr[0]);// 億前面的數字往後塞
		} else {
			unitMath.add(1, "零");// 沒有億則塞零
		}
		if (unitPosition1 > 0) {
			String[] unitStr = unitMath.get(0).split("萬");
			unitMath.set(0, unitStr[1]);// 將value刪除萬
			unitMath.add(1, unitStr[0]);// 萬前面的數字往後塞
		} else {
			unitMath.add(1, "零");// 沒有萬則塞零
		}
		for (int unitIndex = 0; unitIndex < unitMath.size(); unitIndex++) {
			String value = unitMath.get(unitIndex);
			String ch = "零一二三四五六七八九";
			int TenPosition = value.indexOf("十");
			int HunPosition = value.indexOf("百");
			int thoPosition = value.indexOf("千");
			long mathValue = 0;
			if (thoPosition > 0) {
				String Ch = value.substring(thoPosition - 1, thoPosition);// 取得千位的數字
				int Ma = ch.indexOf(Ch);// 千位數字轉國字
				if (Ma == -1)
					Ma = 2;// 兩
				mathValue += Ma * 1000;
			}
			if (HunPosition > 0) {
				String Ch = value.substring(HunPosition - 1, HunPosition);// 取得百位的數字
				int Ma = ch.indexOf(Ch);// 百位數字轉國字
				if (Ma == -1)
					Ma = 2;// 兩
				mathValue += Ma * 100;
			}
			if (TenPosition > 0) {
				String Ch = value.substring(TenPosition - 1, TenPosition);// 取得十位的數字
				int Ma = ch.indexOf(Ch);// 十位數字轉國字
				if (Ma == -1)
					Ma = 2;// 兩
				mathValue += Ma * 10;
			}
			if (TenPosition == value.length() - 1 || HunPosition == value.length() - 1
					|| thoPosition == value.length() - 1) {

			} else {// 十百千都不在最後一位則有個位數
				String Ch = value.substring(value.length() - 1, value.length());// 取得個位的數字
				int Ma = ch.indexOf(Ch);// 個位數字轉國字
				mathValue += Ma * 1;
			}
			mathValueTotal += mathValue * Math.pow(10000, unitIndex);// 依照位置為萬單位
		}
		// 沒有單位的轉換
		String ch = "零一二三四五六七八九";
		long mathValueTotal1 = 0;
		int powIndex = 0;
		for (int index = value1.length(); index > 0; index--) {
			int position = ch.indexOf(value1.substring(index - 1, index));
			if (position >= 0) {
				mathValueTotal1 += position * Math.pow(10, powIndex++);
			} else if(value1.substring(index - 1, index).equalsIgnoreCase("兩")) {// 兩
				mathValueTotal1 += 2 * Math.pow(10, powIndex++);
			}
		}
		if (mathValueTotal1 > mathValueTotal)
			return mathValueTotal1;
		else
			return mathValueTotal;
	}

	// 數字轉國字(不包含單位..十百千萬億兆)
	private static String DateintToStr(long value) {// 數字需要去除零零
		String[] ch = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
		int One = (int) (value % 10);
		int Ten = (int) (value % 100 / 10);
		int Hun = (int) (value % 1000 / 100);
		int tho = (int) (value % 10000 / 1000);
		int pos = 0;
		String chStr = "";
		if (tho > 0) {// 千位
			chStr = ch[tho] + ch[Hun] + ch[Ten] + ch[One];
		} else if (Hun > 0) {// 百位
			chStr = ch[Hun] + ch[Ten] + ch[One];
		} else if (Ten > 0) {// 十位
			chStr = ch[Ten] + ch[One];
		} else {
			chStr = ch[One];
		}
		if (value >= 10000) {
			chStr = DateintToStr(value / 10000) + chStr;
		}
		return chStr;
	}

	// 數字轉國字(包含單位..十百千萬億兆)
	private static String intToStr(long value, int unitInit) {// 數字需要去除
		String[] ch = { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
		String[] unit = { "", "萬", "億", "兆" };
		int One = (int) (value % 10);
		int Ten = (int) (value % 100 / 10);
		int Hun = (int) (value % 1000 / 100);
		int tho = (int) (value % 10000 / 1000);
		int pos = 0;
		String chStr = ch[tho] + "千" + ch[Hun] + "百" + ch[Ten] + "十" + ch[One];
		while (chStr.subSequence(0, 1).equals("零") && chStr.length() > 1) {// 去頭"零"
			chStr = chStr.substring(2, chStr.length());
		}
		if (chStr.subSequence(chStr.length() - 1, chStr.length()).equals("零") && chStr.length() > 1) {
			chStr = chStr.substring(0, chStr.length() - 1);
		}
		while (chStr.length() > 1 && chStr.subSequence(chStr.length() - 2, chStr.length() - 1).equals("零")) {// 去尾"零"
			chStr = chStr.substring(0, chStr.length() - 2);
		}
		if (chStr.length() > 1 && chStr.subSequence(0, 2).equals("一十")) {
			chStr = chStr.substring(1, chStr.length());
		}
		chStr = chStr.replace("零千", "零");
		chStr = chStr.replace("零百", "零");
		chStr = chStr.replace("零十", "零");
		chStr = chStr.replace("零零", "零");
		chStr = chStr.replace("二百", "兩百");
		chStr = chStr.replace("二千", "兩千");
		if (value >= 10000) {
			chStr = intToStr(value / 10000, ++unitInit) + unit[unitInit] + chStr;
		}
		return chStr;
	}
}
