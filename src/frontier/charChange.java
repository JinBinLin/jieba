package frontier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class charChange
 */
@WebServlet("/charChange")
public class charChange extends HttpServlet {
	private static final String keywordColumn = "keyword";
	private static final String vdwordColumn = "vdkey";
	private static final String idColumn = "rid";
	private static int count = 0;
	private static int countMax = 0;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");// 設定網頁丟出需求時，用UTF-8編碼
		response.setCharacterEncoding("UTF-8");// 列出訊息時，用UTF-8編碼
		if (request.getParameter("tableName") != null) {
			if (countMax == 0) {
				resetTabelData(request.getParameter("tableName"));
				response.getWriter().append("ok");
			} else {
				response.getWriter().append("is run : " + count + "/" + countMax);
			}
		}else {
			if (countMax == 0) {
				response.getWriter().append("isn't run : " + count + "/" + countMax);
			} else {
				response.getWriter().append("is run : " + count + "/" + countMax);
			}
		}
	}

	private void resetTabelData(final String tableName) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				String sql = "SELECT * FROM " + tableName;
				readDB DB = new readDB("learn");
				ArrayList<HashMap<String, String>> data = DB.getDBresult(sql);
				countMax = data.size();
				for (HashMap<String, String> item : data) {
					count++;
					// 取出word
					String keyword = item.get(keywordColumn);
					String vdword = item.get(vdwordColumn);
					if(vdword==null)
						vdword = keyword;
					String id = item.get(idColumn);
					// 轉換
					keyword = transform(keyword);
					vdword = transform(vdword);
					vdword = removeSymbol(vdword);
					// 寫入資料庫
					DB.writeDB("update " + tableName + " set " + keywordColumn + " = '" + keyword + "' ," + vdwordColumn
							+ " = '" + vdword + "' where " + idColumn + " = " + id);
				}
				count = 0;
				countMax = 0;
				DB.closeDB();
				DB = null;

			}
		}).start();
	}
	//全形轉半形
	private String transform(String charString) {
		for (char c : charString.toCharArray()) {
			charString = charString.replaceAll("　", " ");
			if ((int) c >= 65281 && (int) c <= 65374) {
				charString = charString.replace(c, (char) (((int) c) - 65248));
			}
		}
		return charString;
	}
	//移除符號
	private String removeSymbol(String word) {
		word = word.replaceAll("['。•﹣—「」﹚’﹙﹞﹝]", "");
		return word.replaceAll("\\p{Punct}", "");
	}
}
