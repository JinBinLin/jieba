package frontier;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;

/**
 * Servlet implementation class testMap
 */
@WebServlet("/testMap")
public class testMap extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		mapToJieba();
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	private void mapToJieba() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				readDB db = new readDB();
				ArrayList<HashMap<String, String>> text = db.getDBresult("SELECT text FROM testJieba");
				for(HashMap<String, String> item :text) {
					try {
						long time = Calendar.getInstance().getTimeInMillis();
						JSONObject object =new JSONObject(Jsoup.connect("http://192.168.10.216/jieba/cut/api/v1.0/"+URLEncoder.encode(item.get("text"),"UTF-8")).timeout(60*1000).execute().body());
						long timeFinish = Calendar.getInstance().getTimeInMillis() - time;
						JSONArray pos = object.getJSONArray("pos");//詞性
						JSONArray cont = object.getJSONArray("cont");//內容
						String posString = "";
						String content = "";
						for(int index = 0 ; index <cont.length();index++) {//將內容以,串接
							posString += pos.getString(index) + ",";
							content += cont.getString(index)+ ",";
						}
						db.writeDB("update testJieba set dismantle='" + content + "',pos='" + posString+"',time="+timeFinish + " where text ='"+item.get("text")+"'");	
						System.out.println(item.get("text")+" : "+content +" : " + posString + " : " + timeFinish);
					} catch (Exception e) {
						System.out.println(item.get("text")+":"+e.toString());
						continue;
					}
				}
				db.closeDB();
				db = null;
			}
		}).start();
	}
}
